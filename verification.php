
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>loans</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Loans System </span></a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
            <li class="active"><a href="index.php"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg><span> Dashboard</span></a></li>
			<li><a href="registration.php"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>Register</a></li>
				<li><a href="users.php"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Users</a></li>
			<li><a href="tables.html"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Tables</a></li>
			<li><a href="forms.html"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg> Forms</a></li>
			<li><a href="panels.html"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Alerts &amp; Panels</a></li>
			<li><a href="icons.html"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> Icons</a></li>
			<li class="parent ">
				<a href="#">
					<span data-toggle="collapse" href="#sub-item-1"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg></span> Dropdown 
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 1
						</a>
					</li>
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 2
						</a>
					</li>
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 3
						</a>
					</li>
				</ul>
			</li>
			<li role="presentation" class="divider"></li>
			<li><a href="login.html"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Customer Registration Page</a></li>
		</ul>

	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Dashboard</h1>
			</div>
		</div>
        <!--/.row-->
        <div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Customer Registration page</div>
            <div class="panel-body">
                <div class="canvas-wrapper">
                    <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
                        <div class="login-panel panel panel-default">
                            <div class="panel-heading">Register</div>
                            <div class="panel-body">
                                <form  method="POST" action="">
                                    <fieldset>
                                        <div class="form-horizontal">
                                          <div class="form-group">
                                         <label for="name" class="col-sm-4 control-label">First  Name</label>
                                            <div class="col-sm-8">
                                          <input type="text" class="form-control" id="fname" name="fname" placeholder="First  Name" value=""> </div>
                                           </div>
                                             <div class="form-group">
                                         <label for="name" class="col-sm-4 control-label">Last Name</label>
                                            <div class="col-sm-8">
                                          <input type="text" class="form-control" id="lname" name="lname" placeholder="Last  Name" value=""> </div>
                                           </div>
                                             <div class="form-group">
                                         <label for="name" class="col-sm-4 control-label">Date of Birth</label>
                                            <div class="col-sm-8">
                                          <input type="date" class="form-control" id="dob" name="dob" placeholder="Date of Birth" value=""> </div>
                                           </div>
                                             <div class="form-group">
                                         <label for="name" class="col-sm-4 control-label"> Address</label>
                                            <div class="col-sm-8">
                                          <input type="address" class="form-control" id="physicall" name="physical" placeholder="Physicall address" value=""> </div>
                                           </div>
                                             <div class="form-group">
                                         <label for="name" class="col-sm-4 control-label">Postal Address</label>
                                            <div class="col-sm-8">
                                          <input type="text" class="form-control" id="postal" name="postal" placeholder="Postal Address" value=""> </div>
                                           </div>
                                            <div class="form-group">
                                         <label for="phone" class="col-sm-4 control-label">Phone No</label>
                                            <div class="col-sm-8">
                                          <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" value=""> </div>
                                           </div>
                                            <div class="form-group">
		                                  <div class="col-sm-10 col-sm-offset-2">
			                             <input id="submit" name="submit" type="submit" value="Register" class="btn btn-primary">
	                                       	</div>
	                                       </div>
                                        </div>
                                 
                                    </fieldset>
                                </form>
                                       <?php
                         if (isset($_POST["submit"])) {
                     $fname = $_POST['fname'];
                     $lname= $_POST['lname'];
                     $dob = $_POST['dob'];
                    $physical = $_POST['physical'];
                    $postall = $_POST['postal'];
                    $phone_no = $_POST['phone'];
//                    if(empty($fname) || empty( $lname) || empty( $dob ) || empty($physical) || empty($postall ) || empty($email) || empty($phone_no) )
//                                {
//                               echo "You did not fill out the required fields.";
//                           die();  // Note this
////                               }
//                             else{
                            //nclude 'include.php';
                                $host="localhost";
                                $user="loans";
                               $pass="password";
                                $db="loan";
                             
                               $mysqli = new mysqli($host, $user, $pass, $db);

                               if($mysqli->connect_error) 
                              die('Connect Error (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
 
                                 $in=mysqli_query($mysqli,"INSERT INTO customer (firstname, lastname,physicalladdress, postaladdress, mobilenumber) 
                               VALUES ('$fname', '$lname',  '$physical', '$postall', '$phone_no') ");  
                             if($in){
                                 echo "Successfully registered";
                             }
                             else{
                                 echo "Not registered. Contact system admin";
                             }
                    
                            
                            }
                         
                            ?>
                            </div>
                     </div
                  
<!--							<canvas class="main-chart" id="line-chart" height="200" width="600"></canvas>-->
						</div>
					</div>
				</div>
			</div>
		</div>
         <div class="row">
     <div class="col-lg-12">
     <hr>
<div id='footer'>&copy Copyright 2016-2017  <?php echo $domain; ?>
	</div>	
		</div>
	</div>	<!--/.main-->

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>
    </div>
    </div>
    </div>
</body>
</html>
