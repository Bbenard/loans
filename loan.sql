-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 03, 2016 at 06:15 PM
-- Server version: 5.7.13-0ubuntu0.16.04.2
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loan`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customerid` int(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `usertype` varchar(128) NOT NULL,
  `firstname` varchar(128) NOT NULL,
  `lastname` varchar(128) NOT NULL,
  `dob` date DEFAULT NULL,
  `physicalladdress` varchar(120) NOT NULL,
  `postaladdress` varchar(128) NOT NULL,
  `mobilenumber` varchar(20) NOT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `Password` varchar(128) DEFAULT NULL,
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdby` varchar(120) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `verify` int(2) NOT NULL DEFAULT '0',
  `verificationcomments` varchar(1024) DEFAULT NULL,
  `verifiedby` varchar(128) DEFAULT NULL,
  `dateverified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customerid`, `username`, `usertype`, `firstname`, `lastname`, `dob`, `physicalladdress`, `postaladdress`, `mobilenumber`, `Email`, `Password`, `datecreated`, `createdby`, `status`, `verify`, `verificationcomments`, `verifiedby`, `dateverified`) VALUES
(1, '', '', '221', '1212', '2016-11-02', '12121', '1212', '12121', '', NULL, '2016-11-02 10:59:36', NULL, 1, 0, NULL, NULL, NULL),
(2, '', '', '111', 'njhh', '2016-11-03', '15454', '5454', '45454', 'kben@gmail.com', 'Password', '2016-11-03 12:08:55', NULL, 1, 0, NULL, NULL, NULL),
(3, '', '', 'fname', 'lname', NULL, 'physical', 'postall', 'phone_no', '', NULL, '2016-11-02 12:09:40', NULL, 1, 0, NULL, NULL, NULL),
(4, '', '', '5454', '4545', NULL, '4545', '4545', '4545454', '', NULL, '2016-11-02 12:10:42', NULL, 1, 0, NULL, NULL, NULL),
(5, '', '', '5454', '4545', NULL, '4545', '4545', '4545454', '', NULL, '2016-11-02 12:12:37', NULL, 1, 0, NULL, NULL, NULL),
(19, '', '', 'Benard', 'kiprotich', NULL, '50298', '00100', '0724049347', '', NULL, '2016-11-02 15:40:43', NULL, 1, 0, NULL, NULL, NULL),
(29, '', '', 'Benard', 'kiprotich', NULL, '50298', '00100', '+254724049347', 'benkip69@gmail.com', 'Password', '2016-11-03 13:17:05', NULL, 1, 1, NULL, NULL, NULL),
(30, ' ', '', '', '', NULL, '', '', '', '', '', '2016-11-03 12:15:48', NULL, 1, 0, NULL, NULL, NULL),
(31, ' Bbenard', 'admin', 'Benard', 'kiprotich', NULL, '67 bomet', '34 bomet', '0724049347', 'benah1013@gmail.com', '1233445555778', '2016-11-03 12:16:42', NULL, 1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customerscore`
--

CREATE TABLE `customerscore` (
  `scoreid` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `score` varchar(6) NOT NULL,
  `createdby` varchar(120) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `scoredby` int(11) NOT NULL,
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loan`
--

CREATE TABLE `loan` (
  `loanid` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `periodid` int(2) NOT NULL,
  `loanamount` varchar(11) NOT NULL,
  `preference` varchar(128) NOT NULL,
  `comments` varchar(512) NOT NULL,
  `disbursementstatus` int(2) DEFAULT NULL,
  `status` int(2) DEFAULT '0',
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdby` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loan`
--

INSERT INTO `loan` (`loanid`, `customerid`, `periodid`, `loanamount`, `preference`, `comments`, `disbursementstatus`, `status`, `datecreated`, `createdby`) VALUES
(1, 2, 3, '20000', '1', 'i need this loan asap', NULL, 0, '2016-11-03 14:08:26', NULL),
(2, 2, 3, '20000', '1', 'i need this loan asap', NULL, 0, '2016-11-03 14:08:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loanlimit`
--

CREATE TABLE `loanlimit` (
  `limitid` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `score` int(3) NOT NULL,
  `loanlimit` varchar(128) NOT NULL,
  `comments` varchar(512) DEFAULT NULL,
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0',
  `createdby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loanlimit`
--

INSERT INTO `loanlimit` (`limitid`, `customerid`, `score`, `loanlimit`, `comments`, `datecreated`, `status`, `createdby`) VALUES
(1, 29, 4, '40000', 'good profile', '2016-11-03 11:53:31', 0, NULL),
(2, 29, 4, '40000', 'good profile', '2016-11-03 11:53:37', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `periodtype`
--

CREATE TABLE `periodtype` (
  `periodid` int(11) NOT NULL,
  `periodtype` varchar(128) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periodtype`
--

INSERT INTO `periodtype` (`periodid`, `periodtype`, `status`) VALUES
(1, 'daily', 1),
(2, 'weekly', 1),
(3, 'monthly', 1),
(4, 'yearly', 1);

-- --------------------------------------------------------

--
-- Table structure for table `repayment`
--

CREATE TABLE `repayment` (
  `repaymentid` int(11) NOT NULL,
  `loanid` int(11) NOT NULL,
  `repaymentdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(2) NOT NULL,
  `checkedby` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `usertype` varchar(128) NOT NULL,
  `idnumber` varchar(128) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` int(128) NOT NULL,
  `Password` varchar(128) DEFAULT NULL,
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addedby` varchar(200) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `usertype`, `idnumber`, `email`, `phone`, `Password`, `datecreated`, `addedby`, `status`) VALUES
(1, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-02 22:50:55', NULL, 1),
(2, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:08:57', NULL, 1),
(3, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:10:30', NULL, 1),
(4, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:10:35', NULL, 1),
(5, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:11:14', NULL, 1),
(6, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:11:52', NULL, 1),
(7, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:12:45', NULL, 1),
(8, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:14:12', NULL, 1),
(9, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:14:50', NULL, 1),
(10, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:15:27', NULL, 1),
(11, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:16:44', NULL, 1),
(12, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:16:49', NULL, 1),
(13, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:18:31', NULL, 1),
(14, 'ben', 'admin', '111', 'benkip69@yahoo.com', 122, '', '2016-11-03 08:19:28', NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customerid`);

--
-- Indexes for table `customerscore`
--
ALTER TABLE `customerscore`
  ADD PRIMARY KEY (`scoreid`);

--
-- Indexes for table `loan`
--
ALTER TABLE `loan`
  ADD PRIMARY KEY (`loanid`);

--
-- Indexes for table `loanlimit`
--
ALTER TABLE `loanlimit`
  ADD PRIMARY KEY (`limitid`);

--
-- Indexes for table `periodtype`
--
ALTER TABLE `periodtype`
  ADD PRIMARY KEY (`periodid`);

--
-- Indexes for table `repayment`
--
ALTER TABLE `repayment`
  ADD PRIMARY KEY (`repaymentid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customerid` int(128) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `customerscore`
--
ALTER TABLE `customerscore`
  MODIFY `scoreid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `loan`
--
ALTER TABLE `loan`
  MODIFY `loanid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `loanlimit`
--
ALTER TABLE `loanlimit`
  MODIFY `limitid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `periodtype`
--
ALTER TABLE `periodtype`
  MODIFY `periodid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `repayment`
--
ALTER TABLE `repayment`
  MODIFY `repaymentid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
